ARG VARIANT=8
ARG OS=debian

FROM registry.gitlab.com/nexylan/docker/core:3.1.0 as core
FROM composer:2.6.6 as composer

FROM php:${VARIANT} as build-debian
RUN apt-get update && apt-get install --yes --no-install-recommends unzip=6.* \
  && apt-get clean \
  && rm --recursive --force /var/lib/apt/lists/*

FROM php:${VARIANT}-alpine as build-alpine
RUN apk add --no-cache unzip~=6

# @see https://github.com/hadolint/hadolint/issues/219
# hadolint ignore=DL3006
FROM build-${OS} as build
COPY --from=core / /
RUN setup
COPY rootfs /
RUN configure
COPY --from=composer /usr/bin/composer /usr/bin/composer
RUN composer global config --no-plugins allow-plugins.bamarni/composer-bin-plugin true \
  && composer global config --no-plugins allow-plugins.sllh/composer-versions-check true \
  && composer global require \
  --prefer-dist --no-progress --no-interaction --no-cache \
  bamarni/composer-bin-plugin:1.8.0 \
  sllh/composer-versions-check:2.0.5
WORKDIR /app

FROM registry.gitlab.com/nexylan/docker/docker:1.6.0 as test
RUN apk add --no-cache bats~=1
WORKDIR /app
CMD ["bats", "."]
COPY docker-compose.yml .
COPY tests.bats .
