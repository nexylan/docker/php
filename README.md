# PHP

On steroid PHP instance.

## Requirements

- Docker

## Usage

Use our image on your Dockerfile:

```Dockerfile
FROM registry.gitlab.com/nexylan/docker/php:x.y.z
# Will be copied to the current /app working directory
COPY . .
CMD php --server 0.0.0.0:80 -t public
```

### Tag usage

The image tags follow format `{version}-{php-version}(-{php-variant})`:

- `version`: The nexylan release.
- `php-version`: The PHP version you want to target.
- `php-variant`: Additional variant. Currently supported: `fpm`, `alpine`, `fpm-alpine`.

Here is some examples:

- `1.0.0-7`
- `1.0.0-7.3`
- `1.3.0-7.3-fpm`

## Features

This image inherits from the official PHP image, with some built-in features and improvements.

### Composer

Composer is directly accessible.

The binary come form the [official docker image][docker_hub_composer].

[docker_hub_composer]: https://hub.docker.com/_/composer "Docker Hub"

#### Plugins

Some useful composer plugins are globally installed:

- [bamarni/composer-bin-plugin](https://packagist.org/packages/bamarni/composer-bin-plugin): No conflicts for your bin dependencies.
- [sllh/composer-versions-check](https://packagist.org/packages/sllh/composer-versions-check): Checks if packages are up to date to last major versions after update.

## Development

```
make
```
