#!/usr/bin/env bats

function dcr() {
  docker-compose run --rm app $@
  docker-compose run --rm alpine $@
  docker-compose run --rm fpm $@
  docker-compose run --rm fpm-alpine $@
}

@test "php" {
  dcr php --version
}

@test "composer" {
  dcr composer --version
}

@test "unzip" {
  dcr unzip --help
}

function check_module() {
  docker-compose run --rm app php -m | grep --extended-regexp --word-regexp "^${1}"
  docker-compose run --rm alpine php -m | grep --extended-regexp --word-regexp "^${1}"
  docker-compose run --rm fpm php -m | grep --extended-regexp --word-regexp "^${1}"
  docker-compose run --rm fpm-alpine php -m | grep --extended-regexp --word-regexp "^${1}"
}

@test "module:gd" {
  check_module gd
}
@test "module:intl" {
  check_module intl
}
@test "module:pcntl" {
  check_module pcntl
}
@test "module:mbstring" {
  check_module mbstring
}
@test "module:json" {
  check_module json
}
@test "module:curl" {
  check_module curl
}
@test "module:xml" {
  check_module xml
}
@test "module:zip" {
  check_module zip
}
@test "module:pdo_sqlite" {
  check_module pdo_sqlite
}
@test "module:pdo_mysql" {
  check_module pdo_mysql
}
@test "module:pdo_pgsql" {
  check_module pdo_pgsql
}
